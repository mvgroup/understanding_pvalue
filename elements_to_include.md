## Important elements to talk about

- pvalue invention
- problems pvalue and testing was meant to solve
- underlying assumptions of the model
- effects of sample size on pvalue
- show a couple of case scenario / simulations
- correct use of pvalue
- better alternatives than testing and pvalue