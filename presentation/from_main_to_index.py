#!/usr/bin/env python
# coding: utf-8

# In[10]:


with open('handwriting_lines.txt') as f:
    read_handwriting_lines = f.read()
f.closed


# In[11]:


#print(read_handwriting_lines)


# In[3]:


with open('main.html', 'r') as f:
    html_lines = f.read()
f.closed


# In[4]:


last_line_to_find = "{ src: 'reveal.js/plugin/notes/notes.js', async: true }"
index_last_line_to_find = html_lines.find(last_line_to_find)


# In[5]:


first_part_html = html_lines[:(index_last_line_to_find+len(last_line_to_find))]
second_part_html = html_lines[(index_last_line_to_find+len(last_line_to_find)):]


# In[6]:


assert second_part_html.find("],") == 3, "space between lines is not 3"


# In[7]:


final_html = first_part_html + read_handwriting_lines + second_part_html[5:]

with open('index.html', 'w') as f:
    f.write(final_html)
f.closed


# In[ ]:

print("conversion successful!")


