// TO COMPILE THE PRESENTATION RUN:
// bundle exec asciidoctor-revealjs main.adoc


// aesthetics-related settings
:revealjs_theme: white
:customcss: assets/mv_theme.css
:revealjs_parallaxBackgroundImage: assets/images/background-bg.jpg
:revealjs_parallaxBackgroundSize: 3500px 2300px
:stem: latexmath


= Diving into the p-value: From the 1900's until the ML era
// images settings
:imagesdir: assets/images/


//title slide content
by: https://marcodallavecchia.github.io/biologistsadventure/[Marco Dalla Vecchia] and https://twitter.com/valefonsecadiaz[Valeria Fonseca Diaz]

// START OF SLIDES
// it needs to have empty line between include statements

include::presentation_parts/01_history.adoc[]

include::presentation_parts/03_simulations.adoc[]

include::presentation_parts/05_current_research_practice.adoc[]

include::presentation_parts/04_simulations2.adoc[]

include::presentation_parts/06_statistical_learning_ml.adoc[]

include::presentation_parts/05_paradox_false_discovery.adoc[]

include::presentation_parts/07_closing.adoc[]
