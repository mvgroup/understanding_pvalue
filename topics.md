# Understanding p-value

## Topics to include in presentation

- History of p-value:
    + Fisher biologist -> Genetics, evolution, heredity, physchology
    + Maximizing the chance to obtain the true correct model -> ML paradigm to solve these problems in statistics
    + Significance testing framework to decide on hypothesis on all matters of experimentation : whatever question you have, mathematics tell you whether it's true or not
    + Criticism: we are getting a whether or not information (no additional info on the interpretation or absolute importance), which can happen with associations of non-related variables. It's a phylosophilal question not a scientific one.
    + The concept of Significance and p-value have shadowed the potential of the models behind.


- We are using p-values wrong
    + assumptions are not respected
    + assumptions are correct -> model doesn't fit the data
    + N number of cells instead of conditions
    + lack of proper experimental design -> induces bias and correlation vs causation confusion
    + breaking down the model into individual tests -> multiple testing -> measure a lot of variables on the same experiments -> some will be significant
    + people don't have a correct model for the question

- Simulation / Example of wrong use of p-value
    + unrelated variable with increasing sample size
    + bimodal distribution comparing means
    + simulate test with different conditions done in different days

- Paradox of false discoveries

- Don't focus on a VALUE but on the model
    + There are more modern methods
    + You are still relying on a cutoff YES or NO answer
    + It's arbitrary
    + Doesn't really tell you anything about the truth behind your experiment

- Example of alternatives / good model usage

- Conclusions
    + Don't focus of a VALUE
    + Understand what's behind
    + Embrace more modern models or methods


Other thoughts Valeria
Alternative:

Build models and as other researchers investigate the same phenomenon, let the models change or adapt according to new findings on the phenomena

Get the paradoxes sink in for students
