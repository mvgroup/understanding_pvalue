# Understanding p-value
## History, uses and potential misuses of p-values in scientific research

![pvalue](presentation/assets/images/series1_sample_size.png)

We share here a discussion about the p-value including some examples and its dependency on the sample size.
Find the presentation if this discussion at https://mvgroup.gitlab.io/mvlearn/presentation-understanding-pvalue/presentation/main.html
